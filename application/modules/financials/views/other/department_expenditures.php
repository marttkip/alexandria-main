<?php echo $this->load->view('search/search_department_expenditure','', true);?>

<?php
 $department_id  = $this->session->userdata('searched_department_id');
 $result = '';
 if(!empty($department_id))
 {
    $department_query = $this->company_financial_model->get_department_expenses($department_id);
    $x=0;
    $balance = 0;
    if($department_query->num_rows() > 0)
    {
      foreach ($department_query->result() as $key => $value) {
        // code...
        $transactionClassification = $value->transactionClassification;

        $document_number = '';
        $transaction_number = '';
        $finance_purchase_description = '';
        $finance_purchase_amount = 0 ;


         $referenceId = $value->payingFor;
        $document_number =$transaction_number = $value->referenceCode;
        $finance_purchase_description = $value->transactionName;

        $cr_amount = $value->cr_amount;
        $dr_amount = $value->dr_amount;


        $transaction_date = $value->transactionDate;
        $transaction_date = date('jS M Y',strtotime($transaction_date));
        // $creditor_name = $value->creditor_name;
        $creditor_id = 0;//$value->creditor_id;
        $account_name = $value->accountName;
        $finance_purchase_id = '';//$value->finance_purchase_id;


        $balance += $dr_amount;
        $balance -=  $cr_amount;
        $x++;
         $result .= '
                   <tr>
                        <td class="text-left">'.$x.'</td>
                       <td class="text-left">'.$transaction_date.'</td>
                       <td class="text-left">'.$account_name.'</td>
                       <td class="text-left">'.$finance_purchase_description.'</td>

                       <td class="text-right">'.number_format($dr_amount).'</td>
                       <td class="text-right">'.number_format($balance).'</td>
                   </tr>
                    ';
      }
      $result .= '
                <tr>
                     <td class="text-right" colspan="4">Total</td>
                    <td class="text-right">'.number_format($balance).'</td>
                    <td class="text-right">'.number_format($balance).'</td>
                </tr>
                 ';
    }
 }

$department_id = $this->session->userdata('searched_department_id');
// var_dump($department_id);die();
if(!empty($department_id))
{
  $department_name = $this->company_financial_model->get_department_name($department_id);
  $search_title = $this->session->userdata('department_expense_title_search');

  ?>
  <div class="text-center">
  	<h3 class="box-title"><?php echo $department_name;?> Deparment Expenditure(s)</h3>
  	<h5 class="box-title"><?php echo $search_title?></h5>
  	<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
  </div>
  <div class="text-right">
      <a href="<?php echo base_url().'financials/company_financial/close_department_expenditure';?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Close Search</a>
      <a href="<?php echo base_url().'print-department-expenditure';?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Report</a>
  </div>
  <?php

}
else {
  ?>
  <div class="text-center">

  	<h5 class="box-title">Please Search for something</h5>
  </div>
  <?php
}

?>



<section class="panel">

		<div class="panel-body">
    	<!-- <h3 class="box-title">Revenue</h3> -->
    	<table class="table  table-striped table-condensed">
  			<thead>
  				<tr>
            <th class="text-left">#</th>
              <th class="text-left">Date</th>
        			<th class="text-left">Account</th>
              <th class="text-left">Description</th>
        			<th class="text-right">Amount</th>
  				    <th class="text-right">NET Movement</th>
  				</tr>
  			</thead>
  			<tbody>
  				<?php echo $result?>
  			</tbody>
  		</table>

    </div>
</section>
