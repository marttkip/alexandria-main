<?php 

$rs = $this->nurse_model->get_visit_pan_detail($visit_id);
//$rs2 = $this->nurse_model->get_visit_pan_detail($visit_id);
$num_rows = count($rs);
//$num_rows2 = count($rs2);
//echo $num_rows;
		
echo "<div class='row'>";
if($num_rows > 0)
{
	echo
	"
	<div class='col-md-6'>
		<table class='table table-striped table-hover table-condensed'>
			<tr>
				<th>Plan Name</th>
				<th>Description</th>
			</tr>";

			foreach ($rs as $key):
				$plan_id = $key->plan_id;
				$name = $key->plan_name;
				$description = $key->plan_description;
				
				echo "
				<tr>
					<td>".$description."</td>
					<td>".$name."</td>
				</tr>";
			endforeach;
		echo"</table>
	</div>
 	";
}

?>