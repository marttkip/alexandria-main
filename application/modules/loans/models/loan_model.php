<?php
    class Loan_model extends CI_Model 
    {	

 	public function get_all_loan($table, $where, $config, $page, $order, $order_method = 'ASC')
    {
    		//retrieve all users
    		$this->db->select('*');
    		$this->db->where($where);
    		$this->db->order_by($order, $order_method);
    		$query = $this->db->get($table, $config, $page);
    		
    		return $query;
	}		

 	public function add_loan_details()
	{
		$data = array(
				'loan_id'=>$this->input->post('loan_id'),
				'loan_description'=>$this->input->post('loan_description'),
				'loan_start_date'=>$this->input->post('loan_start_date'),
				'loan_end_date'=>$this->input->post('loan_end_date'),
				'loan_cost'=>$this->input->post('loan_cost'),
				'loan_status'=>1,
				'loan_facility_id'=>$this->input->post('loan_facility_id'),
				'created'=>date('Y-m-d H:i:s')
				
			);
			
		if($this->db->insert('loan', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
     }

    public function update_loan($loan_id)
	{
		$data = array(

				'loan_description'=>$this->input->post('loan_description'),
				'loan_start_date'=>$this->input->post('loan_start_date'),
				'loan_end_date'=>$this->input->post('loan_end_date'),
				'loan_cost'=>$this->input->post('loan_cost'),
				'loan_facility_id'=>$this->input->post('loan_facility_id'),
				'loan_status'=>1,
				'created'=>date('Y-m-d H:i:s')
			);
			
		$this->db->where('loan_id', $loan_id);
		if($this->db->update('loan', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

 	public function get_loan($loan_id)
	{
		//retrieve all users
		$this->db->from('loan');
		$this->db->select('*');
		$this->db->where('loan_id = '.$loan_id);
		$query = $this->db->get();
		
		return $query;    	
 
     }	

    public function get_loan_facility()
	{
		//retrieve all users
		$this->db->from('loan_facility');
		$this->db->select('*');
		$this->db->where('loan_facility_id > 0 ');
		$query = $this->db->get();
		
		return $query;    	
 
     }

   	public function delete_loan($loan_id)
	{
		if($this->db->delete('loan', array('loan_id' => $loan_id)))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

  	}

   	public function activate_loan($loan_id)
	{
		$data = array(
				'loan_status' => 1
			);
		$this->db->where('loan_id', $loan_id);
		
		if($this->db->update('loan', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	 }
	public function deactivate_loan($loan_id)
	{
		$data = array(
				'loan_status' => 0
			);
		$this->db->where('loan_id', $loan_id);
		
		if($this->db->update('loan', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}	 
	  

}	

?>