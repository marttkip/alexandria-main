<?php
    class loan_facility_model extends CI_Model 
    {	

    	public function get_all_loan_facility($table, $where, $config, $page, $order, $order_method = 'ASC')
    	
     {
    		//retrieve all users
    		$this->db->select('*');
    		$this->db->where($where);
    		$this->db->order_by($order, $order_method);
    		$query = $this->db->get($table, $config, $page);
    		
    		return $query;
    	}
    
       public function all_loan_facility()
    	{
    		$this->db->where('loan_facility_name = 1 AND loan_facility_status > 0');
    		$this->db->order_by('loan_facility_name', 'ASC');
    		$query = $this->db->get('loan_facility');
    		
    		return $query;
    
        }
    
      public function update_loan_facility($loan_facility_id)
	{
		$data = array(
				'loan_facility_name'=>$this->input->post('loan_facility_name'),
				'loan_facility_status'=>$this->input->post('loan_facility_status'),
				
			);
			
		$this->db->where('loan_facility_id', $loan_facility_id);
		if($this->db->update('loan_facility', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
    
    
    public function add_loan_facility_detail()
	{
		$data = array(
				'loan_facility_name'=>ucwords(strtolower($this->input->post('loan_facility_name'))),
				'loan_facility_status'=>$this->input->post('loan_facility_status'),
				'created'=>date('Y-m-d H:i:s'),
				'created_by'=>$this->session->userdata('loan_facility_id')
				
			);
			
		if($this->db->insert('loan_facility', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
     }

    public function get_loan_facility()
	
	  {
		//retrieve all users
		$this->db->from('loan_facility');
		$this->db->select('*');
		$this->db->where('loan_facility_id > 0 ');
		$query = $this->db->get();
		
		return $query;    	
 
     }

  public function delete_loan_facility($loan_facility_id)
	{
		if($this->db->delete('loan_facility_branch', array('loan_facility_id' => $loan_facility_id)))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

   }

   public function activate_loan_facility($loan_facility_id)
	{
		$data = array(
				'loan_facility_status' => 1
			);
		$this->db->where('loan_facility_id', $loan_facility_id);
		if($this->db->update('loan_facility', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	 }
   public function deactivate_loan_facility($loan_facility_id)
	{
		$data = array(
				'loan_facility_status' => 0
			);
		$this->db->where('loan_facility_id', $loan_facility_id);
		if($this->db->update('loan_facility', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}	 	 
	
}   	
?>
