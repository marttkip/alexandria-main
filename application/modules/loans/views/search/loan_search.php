 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title pull-right"></h2>

        <h2 class="panel-title">Search Facility name</h2>

    </header>
    
    <!-- Widget content -->
    <div class="panel-body">
        <div class="padd">
            <?php

            echo form_open("loans/loan/search_loan", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                  <div class="form-group" style="width:800px; margin:0 auto;">
                        <label class="col-lg-4 control-label">Facility name: </label>
                        
                       <div class="col-lg-8">
                                <select  name="loan_facility_name" class="form-control">
                                    <option value="">--- None ---</option>
                                    <?php
                                    if($all_categories->num_rows() > 0)
                                    {   
                                        foreach($all_categories->result() as $row):
                                            // $company_name = $row->company_name;
                                            $loan_facility_id = $row->loan_facility_id;
                                            $loan_facility_name = $row->loan_facility_name;
                                            
                                            if($loan_facility_name == set_value('loan_facility_name'))
                                            {
                                                echo "<option value=".$loan_facility_name." selected='selected'> ".$loan_facility_name."</option>";
                                            }
                                            
                                            else
                                            {
                                                echo "<option value=".$loan_facility_name."> ".$loan_facility_name."</option>";
                                            }
                                        endforeach; 
                                    } 
                                    ?>
                                </select>
                            </div>
                    </div>
                </div>

            <br/>
            <div class="center-align">
                <button type="submit" class="btn btn-info btn-sm">Search</button>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>