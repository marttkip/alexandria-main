<section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    </div>
            
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>loan/loans" class="btn btn-info pull-right">Back to loans</a>
                        </div>
                    </div>
                <!-- Adding Errors -->
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! '.$error.' </div>';
            }
			
			//the visit_type details
		    $loan_name = $loan[0]->loan_name;
			$loan_description = $loan[0]->loan_description;
			$loan_start_date = $loan[0]->loan_start_date;
            $loan_end_date = $loan[0]->loan_end_date;
			$loan_cost = $loan[0]->loan_cost;
            $loan_facility_id = $loan[0]->loan_facility_id;
			
            $validation_errors = validation_errors();
            
        
            if(!empty($validation_errors))
            {
                $loan_name= set_value('loan_name');
				$loan_description= set_value('loan_description');
				$loan_start_date= set_value('aloan_start_date');
				$loan_end_date= set_value('loan_end_date');
				$loan_cost= set_value('loan_cost');
				$loan_status= set_value('loan_status');
			
				$loan_id2 = set_value('loan_id');
				
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
            <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
            <div class="row">
                         <div class="col-md-12">
            	<div class="col-md-6">
            	
               <!--  <div class="form-group">
                        <label class="col-lg-4 control-label">Loan Name</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="loan_name" placeholder="description" value="<?php echo $loan_name ;?>" >
                        </div>
                </div> -->
               
                <div class="form-group">
                        <label class="col-lg-4 control-label">Description</label>
                        <div class="col-lg-8">
                            <textarea class="form-control" name="loan_description" placeholder="description" ><?php echo $loan_description ;?></textarea>
                        </div>
                </div>
                
                <div class="form-group">
                        <label class="col-lg-4 control-label">Amount of Money</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="loan_cost" placeholder="description" value="<?php echo $loan_cost ;?>" >
                        </div>
                </div>
               
                      
              </div>
             <div class="col-md-6">
                <div class="form-group">
                        <label class="col-lg-4 control-label">Start Date</label>
                        <div class="col-lg-8">
                           <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker="" class="form-control" name="loan_start_date" placeholder="Purchase date period" value="<?php echo $loan_start_date;?>">
                            </div>
                        </div>
                    </div>
              
                <div class="form-group">
                        <label class="col-lg-4 control-label">End Date</label>
                        <div class="col-lg-8">
                           <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker="" class="form-control" name="loan_end_date" placeholder="" value="<?php echo $loan_end_date;?>">
                            </div>
                        </div>
                   </div>
                  <div class="form-group">
                            <label class="col-lg-4 control-label">Loan facility</label>
                            <div class="col-lg-8">
                                <select id="loan_facility_id" name="loan_facility_id" class="form-control">
                                    <option value="">--- None ---</option>
                                    <?php
                                    if($all_categories->num_rows() > 0)
                                    {	
                                        foreach($all_categories->result() as $row):
										
											$loan_facility_name = $row->loan_facility_name;
											
    										$loan_facility_id2 = $row->loan_facility_id;
    									   if($loan_facility_id2 == $loan_facility_id)
                                            {
                                                echo "<option value=".$loan_facility_id2." selected='selected'> ".$loan_facility_name."</option>";
                                            }
                                            
                                            else
                                            {
                                                echo "<option value=".$loan_facility_id2."> ".$loan_facility_name."</option>";
                                            }
                                    endforeach;	
                                } 
                                ?>
                                </select>
                            </div>
                      </div>                      
                </div>
               </div>   
               
            </div>
            <div class="form-actions center-align" style="margin-top:10px;">
                <button class="submit btn btn-primary" type="submit">
                    Edit Loan
                </button>
            </div>
            <br />
            <?php echo form_close();?>
                </div>
            </section>