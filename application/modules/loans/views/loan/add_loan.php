<section class="panel">
    <header class="panel-heading">

        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    <div class="row" style="margin-bottom:20px;">
                 <div class="col-lg-12">
                        <a href="<?php echo site_url();?>loan/loans" class="btn btn-info btn-sm pull-right">Back to Loans</a>
                  </div>
                </div>
            
          <link href="<?php echo base_url()."assets/themes/jasny/css/jasny-bootstrap.css"?>" rel="stylesheet"/>
          <div class="padd">
            <!-- Adding Errors -->
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }
            
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			$success = $this->session->userdata('success_message');
			$error = $this->session->userdata('error_message');
			
			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}

			?>
		 <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
            <div class="row">
            <div class="col-md-12">
            	<div class="col-md-6">
                      <<!-- div class="form-group">
                        <label class="col-lg-4 control-label">Loan Name</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="loan_name" placeholder="Name" value="<?php echo set_value('loan_name');?>" >
                        </div>
                </div>   -->
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Description</label>
                        <div class="col-lg-8">
                            <textarea class="form-control" name="loan_description" placeholder="Description"> <?php echo set_value('loan_description');?> </textarea>
                        </div>
                </div>  
                <div class="form-group">
                        <label class="col-lg-4 control-label">Amount of Money</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="loan_cost" placeholder="Loan Cost" value="<?php echo set_value('loan_cost');?>" >
                        </div>
                </div>  
            	 
              </div>
             <div class="col-md-6">
                 <div class="form-group">
                        <label class="col-lg-4 control-label">Start Date</label>
                        <div class="col-lg-8">
                           <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker="" class="form-control" name="loan_start_date" placeholder="loan_start_date" value="<?php echo set_value('loan_start_date');?>">
                            </div>
                        </div>
                  </div>   
              
                 <div class="form-group">
                        <label class="col-lg-4 control-label">End Date</label>
                        <div class="col-lg-8">
                           <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker="" class="form-control" name="loan_end_date" placeholder="loan_end_date" value="<?php echo set_value('loan_end_date');?>">
                            </div>
                        </div>
                  </div>
                     <div class="form-group">
                            <label class="col-lg-4 control-label">Loan Facility </label>
                            <div class="col-lg-8">
                                <select id="loan_facility_id" name="loan_facility_id" class="form-control">
                                    <option value="">--- None ---</option>
                                    <?php
                                    if($all_categories->num_rows() > 0)
                                    {	
                                        foreach($all_categories->result() as $row):
											// $company_name = $row->company_name;
											$loan_facility_id = $row->loan_facility_id;
											$loan_facility_name = $row->loan_facility_name;
											
											if($loan_facility_name == set_value('loan_facility_name'))
											{
                                        		echo "<option value=".$loan_facility_id." selected='selected'> ".$loan_facility_name."</option>";
											}
											
											else
											{
                                        		echo "<option value=".$loan_facility_id."> ".$loan_facility_name."</option>";
											}
                                        endforeach;	
                                    } 
                                    ?>
                                </select>
                            </div>
                      </div>           
                </div>
               </div>   
             <div class="form-actions center-align" style="margin-top:10px;">
                        <button class="submit btn btn-primary" type="submit">
                            Add Loan
                        </button>
                </div>    
           </div> 
        </form>

</section>
