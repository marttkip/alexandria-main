<?php
	$result = '';
	
	if($query->num_rows() > 0)
	{
		$count  = $page;
		$result .= '
		            <table class="table table-bordered table-striped table-condensed">
		                <thead>
		                    <tr>
		                        <th>#</th>
		                        <th>Facility Name</th>
		                        <th>Description</th>
		                        <th>Start Date</th>
		                        <th>End Date</th>
		                        <th>Cost</th>
		                        <th>status</th>
		                        <th>Created</th>
		                        <th colspan="9">Actions</th>
		                    </tr>
		                </thead>
		                  <tbody>
		                  
		            ';

			foreach ($query->result() as $row) {
			    $loan_id          = $row->loan_id;
			    $loan_name        = $row->loan_name;
			    $loan_status      = $row->loan_status;
			    $loan_description = $row->loan_description;
			    $loan_start_date  = $row->loan_start_date;
			    $loan_cost        = $row->loan_cost;
			    $loan_end_date    = $row->loan_end_date;
			    $loan_facility_name= $row->loan_facility_name;
			    $created          = date('jS M Y H:i a', strtotime($row->created));
			    $count++;
			    //create deactivated status display
			    if ($loan_status == 0) {
			        $status = '<span class="label label-default">Deactivated</span>';
			        $button = '<a class="btn btn-info btn-sm" href="' . site_url() . 'loan/activate-loan/' . $loan_id . '" onclick="return confirm(\'Do you want to activate ' . $loan_name . '?\');" title="Activate ' . $loan_name . '"><i class="fa fa-thumbs-up"></i> Activate</a>';
			    }
			    //create activated status display
			    else if ($loan_status == 1) {
			        $status = '<span class="label label-success">Active</span>';
			        $button = '<a class="btn btn-default btn-sm" href="' . site_url() . 'loan/deactivate-loan/' . $loan_id . '" onclick="return confirm(\'Do you want to deactivate ' . $loan_name . '?\');" title="Deactivate ' . $loan_name . '"><i class="fa fa-thumbs-down"></i> Deactivate</a>';
			    }
			    
			    //creators & editors
			    
			    $result .= '
			                    <tr>
			                        <td>' . $count . '</td>
			                        <td>' . $loan_facility_name . '</td>
			                        <td>' . $loan_description . '</td>
			                        <td>' . $loan_start_date . '</td>
			                        <td>' . $loan_end_date . '</td>
			                        <td>' . $loan_cost . '</td>
			                        <td>' . $status . '</td>
			                        <td>' . $created . '</td>
			                        <td><a href="' . site_url() . 'loan/edit-loan/' . $loan_id . '" class="btn btn-sm btn-info" title="Edit ' . $loan_name . '"><i class="fa fa-pencil"></i> Edit</a></td>
			                        <td>' . $button . '</td>
			                        <td><a href="' . site_url() . 'loan/delete-loan/' . $loan_id . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete ' . $loan_name . '?\');" title="Delete ' . $loan_name . '"><i class="fa fa-trash"></i> Delete</a></td>
			                    </tr> 
			                ';
			}

			$result .= '
			                        </tbody>
			                        </table>
                      ';
		}
		

		?>
		<?php echo $this->load->view('search/loan_search', '', TRUE);?>

        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>
        
                <h2 class="panel-title"><?php echo $title;?></h2>
            </header>
            <div class="panel-body">
                <div class="row" style="margin-bottom:20px;">
                    <div class="col-lg-12">
                        <a href="<?php echo site_url();?>loan/add-loan" class="btn btn-success btn-sm pull-right">Add loan</a>
                    </div>
                </div>
                <?php
					$error   = $this->session->userdata('error_message');
					$success = $this->session->userdata('success_message');

					if (!empty($success)) {
					    echo '
					                                        <div class="alert alert-success">' . $success . '</div>
					                                    ';
					    $this->session->unset_userdata('success_message');
					}

					if (!empty($error)) {
					    echo '
					                                        <div class="alert alert-danger">' . $error . '</div>
					                                    ';
					    $this->session->unset_userdata('error_message');
					}
					?>
				<div style="min-height:30px;">
            	
            	<div class="pull-right"> 
                	<?php
					$search = $this->session->userdata('search_loan');
		
					if(!empty($search))
					{
						echo '<a href="'.site_url().'loans/loan/close_search_loan" class="btn btn-warning btn-sm">Close Search</a>';
					}
					?>
                </div>
           
            </div>
               <div class="table-responsive">
                    
                    <?php echo $result;?>
           
                </div>
            </div>
            
            <div class="panel-foot">
                
                <?php
					if (isset($links)) {
					    echo $links;
					}
					?>
           
                <div class="clearfix"></div> 
            
            </div>
        </section>
