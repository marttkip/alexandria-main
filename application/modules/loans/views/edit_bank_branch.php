<section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    </div>
            
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>loans/bank-branch" class="btn btn-info pull-right">Back to Asset Category</a>
                        </div>
                    </div>
                <!-- Adding Errors -->
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! '.$error.' </div>';
            }
			
			//the visit_type details
			$bank_branch_name = $bank_branch[0]->bank_branch_name;
			$bank_branch_status = $bank_branch[0]->bank_branch_status;
			$bank_branch_id2 = $bank_branch[0]->bank_branch_id;
            
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
				$bank_branch_name= set_value('bank_branch_name');
				$bank_branch_status = set_value('bank_branch_status');
				$bank_branch_id2 = set_value('bank_branch_id');
				
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
            <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Edit bank branch:</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="bank_branch_name" placeholder="Bank Branch" value="<?php echo $bank_branch_name;?>" required>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="form-actions center-align" style="margin-top:10px;">
                <button class="submit btn btn-primary" type="submit">
                    Edit Bank Branch
                </button>
            </div>
            <br />
            <?php echo form_close();?>
                </div>
            </section>