 <?php  
		$result = ''; 
		if($query->num_rows() > 0)
		{
			$count = $page;
			$result .= 
						'
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>#</th>
									<th>Bank Name</th>
									<th>Created</th>
									<th>Status</th>
									<th colspan="5">Actions</th>
								</tr>
							</thead>
							  <tbody>
							  
						';

   			foreach ($query->result() as $row)
			{
				$loan_facility_id = $row->loan_facility_id;
				$loan_facility_name = $row->loan_facility_name;
				$loan_facility_status = $row->loan_facility_status;
				//$created_by = $row->created_by;
				//$created = date('jS M Y H:i a',strtotime($row->created));
				
				//create deactivated status display
				if($loan_facility_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info btn-sm" href="'.site_url().'bank/activate-bank/'.$loan_facility_id.'" onclick="return confirm(\'Do you want to activate '.$loan_facility_name.'?\');" title="Activate '.$loan_facility_name.'"><i class="fa fa-thumbs-up"></i> Activate</a>';
				}
				//create activated status display
				else if($loan_facility_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default btn-sm" href="'.site_url().'bank/deactivate-bank/'.$loan_facility_id.'" onclick="return confirm(\'Do you want to deactivate '.$loan_facility_name.'?\');" title="Deactivate '.$loan_facility_name.'"><i class="fa fa-thumbs-down"></i> Deactivate</a>';
				}
				
				//creators & editors
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$loan_facility_name.'</td>
						<td>'.$status.'</td>
						<td><a href="'.site_url().'bank/edit-bank/'.$loan_facility_id.'" class="btn btn-sm btn-info" title="Edit '.$loan_facility_name.'"><i class="fa fa-pencil"></i> Edit</a></td>
						<td>'.$button.'</td>
						<td><a href="'.site_url().'bank/delete-bank/'.$loan_facility_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$loan_facility_name.'?\');" title="Delete '.$loan_facility_name.'"><i class="fa fa-trash"></i> Delete</a></td>
					</tr> 
				';
			}
			
			$result .= 
			'
						</tbody>
						</table>
			';
		}
		
			?>
	<?php echo $this->load->view('search/loan_facility_search', '', TRUE);?>

	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
			</div>
	
			<h2 class="panel-title"><?php echo $title;?></h2>
		</header>

		<div class="pull-right"> 
                	<?php
					$search = $this->session->userdata('search_loan_facility');
		
					if(!empty($search))
					{
						echo '<a href="'.site_url().'loans/loan_facility/close_search_loan_facility" class="btn btn-warning btn-sm">Close Search</a>';
					}
					?>
                </div>
		<div class="panel-body">
        	<div class="row" style="margin-bottom:20px;">
                <div class="col-lg-12">
                	<a href="<?php echo site_url();?>loan/add-loan-facility" class="btn btn-success btn-sm pull-right">Add Loan Facility</a>
                </div>
            </div>
            <?php
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');
			
			if(!empty($success))
			{
				echo '
					<div class="alert alert-success">'.$success.'</div>
				';
				$this->session->unset_userdata('success_message');
			}
			
			if(!empty($error))
			{
				echo '
					<div class="alert alert-danger">'.$error.'</div>
				';
				$this->session->unset_userdata('error_message');
			}
			?>

           
			<div class="table-responsive">
            	
				<?php echo $result;?>
		
            </div>
		</div>
        
        <div class="panel-foot">
            
			<?php if(isset($links)){echo $links;}?>
        
            <div class="clearfix"></div> 
        
        </div>
	</section>