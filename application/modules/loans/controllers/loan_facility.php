<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    require_once "./application/modules/loans/controllers/loan.php";
    
    class Loan_facility extends loan
    {
    	function __construct()
    	{
    		parent:: __construct();
    		
    
    	}
        
    	/*
    	*
    	*	Default action is to show all the sections
    	*
    	*/
    	public function index() 
    	{
    		$where = 'loan_facility_id > 0';
    		$table = 'loan_facility';
    		$order = 'loan_facility_name';
    		$order_method = 'ASC';
            $search = $this->session->userdata('search_loan_facility');

             if(isset($search))
            {

                $where .= $search;

                // var_dump($where); die();
            }
    		//pagination
    		$segment = 3;
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'loan/loan-facilities';
    		$config['total_rows'] = $this->users_model->count_items($table, $where);
    		$config['uri_segment'] = $segment;
    		$config['per_page'] = 20;
    		$config['num_links'] = 5;
    		
    		$config['full_tag_open'] = '<ul class="pagination pull-right">';
    		$config['full_tag_close'] = '</ul>';
    		
    		$config['first_tag_open'] = '<li>';
    		$config['first_tag_close'] = '</li>';
    		
    		$config['last_tag_open'] = '<li>';
    		$config['last_tag_close'] = '</li>';
    		
    		$config['next_tag_open'] = '<li>';
    		$config['next_link'] = 'Next';
    		$config['next_tag_close'] = '</span>';
    		
    		$config['prev_tag_open'] = '<li>';
    		$config['prev_link'] = 'Prev';
    		$config['prev_tag_close'] = '</li>';
    		
    		$config['cur_tag_open'] = '<li class="active"><a href="#">';
    		$config['cur_tag_close'] = '</a></li>';
    		
    		$config['num_tag_open'] = '<li>';
    		$config['num_tag_close'] = '</li>';
    		$this->pagination->initialize($config);
    		
    		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
            $v_data["links"] = $this->pagination->create_links();
    		$query = $this->loan_facility_model->get_all_loan_facility($table, $where, $config["per_page"], $page, $order, $order_method);
    		
    		//change of order method 
    		if($order_method == 'DESC')
    		{
    			$order_method = 'ASC';
    		}
    		
    		else
    		{
    			$order_method = 'DESC';
    		}
    		
    		$data['title'] = 'Loan Facilities';
    		$v_data['title'] = $data['title'];
    		
    		$v_data['order'] = $order;
    		$v_data['order_method'] = $order_method;
    		$v_data['query'] = $query;
    		$v_data['page'] = $page;
    		$data['content'] = $this->load->view('all_loan_facility', $v_data, true);
    		
    		$this->load->view('admin/templates/general_page', $data);


    	}

       public function search_loan_facility()
            
            {
                $loan_facility_name = $this->input->post('loan_facility_name');
                
                if(!empty($loan_facility_name))
                {
                    $this->session->set_userdata('search_loan_facility', ' AND loan_facility.loan_facility_name LIKE \'%'.$loan_facility_name.'%\'');
                }
                
                redirect('loan/loan-facilities');
            }
    
        public function close_search_loan_facility()
            
            {
                $this->session->unset_userdata('search_loan_facility');
                
                redirect('loan/loan-facilities');
            }

       
        public function add_loan_facility() 
    	{
    		//form validation rules
    		$this->form_validation->set_rules('loan_facility_name', 'loan_facility', 'required|xss_clean');
            
    		//$this->form_validation->set_rules('asset_category_parent', 'Category Parent', 'required|xss_clean');
    		
    		//if form has been submitted
    		if ($this->form_validation->run())
    		{
    			//upload product's gallery images
    			
    			if($this->loan_facility_model->add_loan_facility_detail())
    			{
    				$this->session->set_userdata('success_message', 'Branch added successfully');
    				redirect('loan/loan-facilities');
    			}
    			
    			else
    			{
    				$this->session->set_userdata('error_message', 'Could not add c. Please try again');
    			}
    		}
    		
    		//open the add new category
    		$data['title'] = 'Add Loan Facility';
    		$v_data['title'] = 'Add Loan Facility ';
    		
    		$data['content'] = $this->load->view('add_loan_facility', $v_data, true);
    		$this->load->view('admin/templates/general_page', $data);
    }
    
     
		
		

        public function edit_loan_facility($loan_facility_id) 
    	{
    		//form validation rules
    		$this->form_validation->set_rules('loan_facility_name', 'Branch Name', 'required|xss_clean');
    		//$this->form_validation->set_rules('asset_category_status', 'Asset Status', 'required|xss_clean');
    		
    		//if form has been submitted
    		if ($this->form_validation->run())
    		{
    			
    			if($this->assets_category_model->update_loan_facility($loan_facility_id))
    			{
    				$this->session->set_userdata('success_message', 'Branch updated successfully');
    				redirect('loan/loan-facilities');
    			}
    			
    			else
    			{
    				$this->session->set_userdata('error_message', 'Could not update loan_facility Branch. Please try again');
    			}
    		}
    		
    		//open the add new category
    		$data['title'] = 'Edit Loan Facility';
    		$v_data['title'] = 'Edit Loan Facility';

    		
    		//select the category from the database
    		$query = $this->loan_facility_model->get_loan_facility($loan_facility_id);
    		$v_data['loan_facility_branch'] = $query->result();

    		//var_dump($query);die();
    		
    		if ($query->num_rows() > 0)
    		{
    			$v_data['loan_facility_branch'] = $query->result();
    			//$v_data['all_parent_categories'] = $this->categories_model->all_parent_categories();
    			
    			$data['content'] = $this->load->view('edit_loan_facility', $v_data, true);
    		}
    		
    		else
    		{
    			$data['content'] = 'Branch does not exist';
    		}
    		
    		$this->load->view('admin/templates/general_page', $data);
    	}
    
      public function delete_loan_facility($loan_facility_id)
        	
        {
        		//delete asset category
         $query = $this->loan_facility_model->get_loan_facility($loan_facility_id);
        		
        		if ($query->num_rows() > 0)
        		{
        			$result = $query->result();
        			
        		}
        		$this->loan_facility_model->delete_loan_facility($loan_facility_id);
        		$this->session->set_userdata('success_message', 'loan_facility  has been deleted');
        		redirect('loan/loan_facility');
        	}
    
       public function activate_loan_facility($loan_facility_id)
    	{
    		$this->loan_facility_model->activate_loan_facility($loan_facility_id);
    		$this->session->set_userdata('success_message', 'loan_facility  activated successfully');
    		redirect('loan/loan_facility');
    	} 
    
    	public function deactivate_loan_facility($loan_facility_id)
    	{
    		if($this->loan_facility_model->deactivate_loan_facility($loan_facility_id))
    		{
    			$this->session->set_userdata('success_message', 'loan_facility disabled successfully');
    			redirect('loan/loan_facility');
    		} 		
    
    	}		

    }
        
    
    ?>
