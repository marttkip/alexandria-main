<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loan extends MX_Controller 
{
    	function __construct()
    	{
    		parent:: __construct();

           $this->load->model('loan_facility_model');
           $this->load->model('admin/users_model');
           $this->load->model('site/site_model');
           $this->load->model('admin/sections_model');
           $this->load->model('admin/admin_model');
           $this->load->model('loans/loan_model');
            $this->load->model('hr/personnel_model');

          }

        public function index() 
    	{
    		$where = 'loan_facility.loan_facility_id = loan.loan_facility_id';
    		$table = 'loan,loan_facility';
            $order = 'loan_facility.loan_facility_name';
            $order_method = 'ASC';
    		//$order = 'loan_name';
    		$order_method = 'ASC';
            $search = $this->session->userdata('search_loan_facility');
            if(isset($search))
            {

                $where .= $search;

                // var_dump($where); die();
            }
    		//pagination
    		$segment = 3;
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'loan/loans';
    		$config['total_rows'] = $this->users_model->count_items($table, $where);
    		$config['uri_segment'] = $segment;
    		$config['per_page'] = 20;
    		$config['num_links'] = 5;
    		
    		$config['full_tag_open'] = '<ul class="pagination pull-right">';
    		$config['full_tag_close'] = '</ul>';
    		
    		$config['first_tag_open'] = '<li>';
    		$config['first_tag_close'] = '</li>';
    		
    		$config['last_tag_open'] = '<li>';
    		$config['last_tag_close'] = '</li>';
    		
    		$config['next_tag_open'] = '<li>';
    		$config['next_link'] = 'Next';
    		$config['next_tag_close'] = '</span>';
    		
    		$config['prev_tag_open'] = '<li>';
    		$config['prev_link'] = 'Prev';
    		$config['prev_tag_close'] = '</li>';
    		
    		$config['cur_tag_open'] = '<li class="active"><a href="#">';
    		$config['cur_tag_close'] = '</a></li>';
    		
    		$config['num_tag_open'] = '<li>';
    		$config['num_tag_close'] = '</li>';
    		$this->pagination->initialize($config);
    		
    		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
            $v_data["links"] = $this->pagination->create_links();
    		$query = $this->loan_model->get_all_loan($table, $where, $config["per_page"], $page, $order, $order_method);
    		
    		//change of order method 
    		if($order_method == 'DESC')
    		{
    			$order_method = 'ASC';
    		}
    		
    		else
    		{
    			$order_method = 'DESC';
    		}
    		
    		$data['title'] = 'Loans';
    		$v_data['title'] = $data['title'];
    		
    		$v_data['order'] = $order;
    		$v_data['order_method'] = $order_method;
    		$v_data['query'] = $query;
    		$v_data['page'] = $page;
            $v_data['all_categories'] = $this->loan_model->get_loan_facility();
    		$data['content'] = $this->load->view('loan/all_loan', $v_data, true);
    		
    		$this->load->view('admin/templates/general_page', $data);
    	}

            public function search_loan()
            
            {
                $loan_facility_name = $this->input->post('loan_facility_name');
                // var_dump($loan_facility_name); die();
                if(!empty($loan_facility_name))
                {
                    $search = ' AND loan_facility.loan_facility_name LIKE \'%'.$loan_facility_name.'%\'';
                    $this->session->set_userdata('search_loan', $search);
                }
                
                redirect('loan/loans');
            }
    
        public function close_search_loan()
            
            {
                $this->session->unset_userdata('search_loan');
                
                redirect('loan/loans');
            }

          public function add_loan() 
    	
          {
    		//form validation rules
   
            $this->form_validation->set_rules('loan_description', 'Description', 'required|xss_clean');
    		$this->form_validation->set_rules('loan_start_date', 'Start Date', 'xss_clean');
    		$this->form_validation->set_rules('loan_end_date', 'End Date', 'xss_clean');
            $this->form_validation->set_rules('loan_cost', 'Loan Cost', 'xss_clean');
    		$this->form_validation->set_rules('loan_facility_id', 'Loan Facility', 'xss_clean');
    	
           // var_dump($_POST); die();

    		
    		//if form has been submitted
    		if ($this->form_validation->run())
    		{
    			//upload product's gallery images
    			
    			if($this->loan_model->add_loan_details())
    			{
    				$this->session->set_userdata('success_message', 'Loan Created successfully');
    				redirect('loan/loans');
    			}
    			
    			else
    			{
    				$this->session->set_userdata('error_message', 'Could not add loan. Please try again');
    			}
    		}
    		
    		//open the add new category
    		$data['title'] = 'Add New Loan';
    		$v_data['title'] = 'Add New Loan ';
    		$v_data['all_categories'] = $this->loan_model->get_loan_facility();
    		
    		$data['content'] = $this->load->view('loan/add_loan', $v_data, true);
    		$this->load->view('admin/templates/general_page', $data);
        }


        public function edit_loan($loan_id) 
    	{
    		//form validation rules
            $this->form_validation->set_rules('loan_cost', 'Loan Cost', 'xss_clean');
            $this->form_validation->set_rules('loan_description', 'Description', 'required|xss_clean');
            $this->form_validation->set_rules('loan_start_date', 'Start Date', 'xss_clean');
            $this->form_validation->set_rules('loan_end_date', 'End Date', 'xss_clean');
            $this->form_validation->set_rules('loan_facility_id', 'Loan Facility', 'xss_clean');
    		
    		//if form has been submitted
    		if ($this->form_validation->run())
    		{
    			
    			if($this->loan_model->update_loan($loan_id))
    			{
    				$this->session->set_userdata('success_message', 'Loan updated successfully');
    				redirect('loan/loans');
    			}
    			
    			else
    			{
    				$this->session->set_userdata('error_message', 'Could not update category. Please try again');
    			}
    		}
    		
    		//open the add new category
    		$data['title'] = 'Edit Loan';
    		$v_data['title'] = 'Edit Loan';
    	    $v_data['all_categories'] = $this->loan_model->get_loan_facility();



    		
    		//select the category from the database
    		$query = $this->loan_model->get_loan($loan_id);
    		$v_data['loan'] = $query->result();

    		//var_dump($query);die();
    		
    		if ($query->num_rows() > 0)
    		{
    			$v_data['loan'] = $query->result();
    			//$v_data['all_parent_categories'] = $this->categories_model->all_parent_categories();
    			
    			$data['content'] = $this->load->view('loan/edit_loan', $v_data, true);
    		}
    		
    		else
    		{
    			$data['content'] = 'Loan does not exist';
    		}
    		
    		$this->load->view('admin/templates/general_page', $data);
    	}  


      public function delete_loan($loan_id)
        	
        {
        		//delete category image
        		$query = $this->loan_model->get_loan($loan_id);
        		
        		if ($query->num_rows() > 0)
        		{
        			$result = $query->result();
        			
        		}
        		$this->loan_model->delete_loan($loan_id);
        		$this->session->set_userdata('success_message', 'Loan has been deleted');
        		redirect('loan/loans');
        }
    
       public function activate_loan($loan_id)
    	{
    		$this->loan_model->activate_loan($loan_id);
    		$this->session->set_userdata('success_message', 'Loan activated successfully');
    		redirect('loan/loans');
    	} 
    
    	public function deactivate_loan($loan_id)
    	{
    		$this->loan_model->deactivate_loan($loan_id);
    		$this->session->set_userdata('success_message', 'Loan disabled successfully');
    		redirect('loan/loans');
    
    	}		


}


?>